# Architectural Designs

This section contains designs and documentation of architectural concerns of machine learning platform for TOBIAS projects, it includes:

* [General Architecture](./general-arrchitecture/README.md)
* [Distributed Training for heavy MNL use cases](./distributed-training-architecture/README.md)
* [Machine Learning Operationalization](./mlops-architecture/README.md)