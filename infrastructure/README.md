## Infrastructure deployment

To simplify the management of resources in our Azure subscription, we define and assign policies or role-based access controls across the subscription. With subscription level templates, policies and assigned roles can be applied at the subscription declaratively. You can also create resource groups and deploy resources.

For deployment of templates at the subscription level, we use PowerShell. The Azure portal doesn't support deployment in the subscription level. Although Azure CLI and REST Api can also be used

To deploy your app to an Azure resource (to an app service or to a virtual machine), you need an Azure Resource Manager service connection.

From Azure DevOps -> Project setting -> Service connection: Then click on "New Service Connection".Choose "Azure resource Manager" as type of service connection. Select Service Principal (Automatic), Do not select resource group, leave it empty. Let's see what will happen



* Create Variable
* Add service connection name into var group

[![Build Status](https://dev.azure.com/BU5/machine-learning-pipelines/_apis/build/status/infra-release-pipeline?branchName=master)](https://dev.azure.com/BU5/machine-learning-pipelines/_build/latest?definitionId=11&branchName=master)



## Create a role assignment
To add a role assignment, you must specify three elements: security principal, role definition, and scope. The security principal is you or another user in your directory, the role definition is Virtual Machine Contributor, and the scope is a resource group that you specify.