# get Command Line Args
environment=$1
resourceGroupName=$2

# Check if KeyVault exists
az keyvault show -n "mlpipeline-kv-$environment" -g $resourceGroupName
# Use recover mode keep ML ws's access policies
if [ $? -eq 0 ]; then
    echo "keyvault recovery deployment"
    echo "##vso[task.setvariable variable=createmode]recover"
else
    echo "keyvault default deployment"
    echo "##vso[task.setvariable variable=createmode]default"
fi